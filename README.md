# Intro to programming

Very small intro to programming

## HTML
In HTML wird mit Tags gearbeiten. Ein öffnender (z.B. `<h1>`) und ein schließender Tag (z.B. `</h1>`) bestimmen, wie der Content zwischen den Tags aussieht.
Beispiel:
```html
<h1>Heading 1 / Überschrift 1</h1>
```
<h1>Heading 1 / Überschrift 1</h1>

Hier eine Übersicht über einige hilfreiche Tags:
```html
<h1>Heading 1 / Überschrift 1</h1>
<h2>Heading 2 / Überschrift 2</h2>
<h3>Heading 3 / Überschrift 3</h3>
<h4>Heading 4 / Überschrift 4</h4>
<h5>Heading 5 / Überschrift 5</h5>
<h6>Heading 6 / Überschrift 6</h6>

<p>Ein einfacher Textabschnitt mit Leerzeile (break -> br)</p>
<br>

<ul>
    <li>Das hier ist der das 1. List-Item (li) in einer Unordered-List (ul)</li>
    <li>Das hier ist der das 2. List-Item (li) in einer Unordered-List (ul)</li>
    <li>Das hier ist der das 3. List-Item (li) in einer Unordered-List (ul)</li>
</ul>

<ol>
    <li>Das hier ist der das 1. List-Item (li) in einer Ordered-List (ol)</li>
    <li>Das hier ist der das 2. List-Item (li) in einer Ordered-List (ol)</li>
    <li>Das hier ist der das 3. List-Item (li) in einer Ordered-List (ol)</li>
</ol>
```

<h1>Heading 1 / Überschrift 1</h1>
<h2>Heading 2 / Überschrift 2</h2>
<h3>Heading 3 / Überschrift 3</h3>
<h4>Heading 4 / Überschrift 4</h4>
<h5>Heading 5 / Überschrift 5</h5>
<h6>Heading 6 / Überschrift 6</h6>

<p>Ein einfacher Textabschnitt mit Leerzeile (break -> br)</p>
<br>

<ul>
    <li>Das hier ist der das 1. List-Item (li) in einer Unordered-List (ul)</li>
    <li>Das hier ist der das 2. List-Item (li) in einer Unordered-List (ul)</li>
    <li>Das hier ist der das 3. List-Item (li) in einer Unordered-List (ul)</li>
</ul>

<ol>
    <li>Das hier ist der das 1. List-Item (li) in einer Ordered-List (ol)</li>
    <li>Das hier ist der das 2. List-Item (li) in einer Ordered-List (ol)</li>
    <li>Das hier ist der das 3. List-Item (li) in einer Ordered-List (ol)</li>
</ol>

## JavaScript
Ein Script kann mit einem `script` Tag eingefügt werden. Jede Zeile im Script muss mit einem `;` beendet werden.

```html
<script>
console.log("Hello World");
</script>
```
In der Console in den Dev-Tools (mit F12 öffnen) steht jetzt Hello World
```
Output:

Hello World
```

### Variablen
Eine Variable kann einen Wert speichern. Wir können diesen Wert dann in Rechnungen verwenden.
```js
var number = 0;
console.log(number);
number = number + 2;
console.log(number);
number = number * 3;
console.log(number);
```
```
Output:

0
2
6
```

### If Bedingung
Eine if Bedingung kann einen Teil Code ausführen, wenn eine Bedingung erfüllt wird
```js
var number = 2;
if (number == 0) { // um zu überprüfen ob number 0 ist, brauchen wir ein ==
    console.log("number ist 0");
}
else if (number >= 5) {
    console.log("number ist größer gleich 5");
}
else if ((number % 2) == 0) {
    console.log("Wenn man number durch 2 teilt, kommt ein Rest von 0 heraus");
}
else {
    console.log("Keiner der Fälle oben trifft zu");
}

```
```
Output:

Wenn man number durch 2 teilt, kommt ein Rest von 0 heraus
```

#### For-Schleife
Eine for-Schleife hat folgende bestandteile
```js
for (Anfangszustand; Bedingung, am Anfang eines Durchlaufs überprüft; Code, am Ende eines Durchlaufs ausgeführt) {
    Schleifeninhalt
}
```

Hier eine Beispielschleife, die von 0 bis 4 zählt
```js
for (var i = 0; i < 5; i = i + 1) {
    console.log(i);
}
```
```
Output:

0
1
2
3
4
```